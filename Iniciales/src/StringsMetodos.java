import java.util.Arrays;

public class StringsMetodos {
    public static void main(String[] args) {

        String a = " ";
        String b = "Peter";

        // Retorna true si,y solo si, length() es 0.
        // return value.length == 0;
        System.out.println("a.isEmpty(): " + a.isEmpty());

        // Retorna true si el string es vacío o contiene solamente espacios en blanco.
        // return indexOfNonWhitespace() == length();
        System.out.println("a.isBlank(): " + a.isBlank());
        System.out.println("------------------------------");

        // Determina el largo de la cadena
        System.out.println("b.length(): " + b.length());
        System.out.println("------------------------------");

        // Convierte toda la cadena a mayúscula
        System.out.println("b.toUpperCase(): " + b.toUpperCase());
        System.out.println("------------------------------");

        // Convierte toda la cadena a minúscula
        System.out.println("b.toLowerCase(): " + b.toLowerCase());
        System.out.println("------------------------------");

        // Compara un string con un objeto específico a nivel de valor.
        System.out.println("b.equals(\"Peper\"): " + b.equals("Peter"));
        System.out.println("b.equals(\"peter\"): " + b.equals("peter"));
        System.out.println("------------------------------");

        // Compara un string con otro string, ignora mayúsculas/minúsculas.
        System.out.println("b.equalsIgnoreCase(\"peter\"): " + b.equalsIgnoreCase("peter"));
        System.out.println("------------------------------");

        // Compara dos cadenas lexicográficamente. La comparación se basa en el valor Unicode de cada carácter de las cadenas.
        // La secuencia de caracteres representada por este objeto String se compara lexicográficamente con la secuencia de caracteres representada por la cadena de argumentos.
        System.out.println("b.compareTo(\"Peter\"): " + b.compareTo("Peter"));

        // A(65) - B(66)
        System.out.println("A(65) - B(66): " + "A".compareTo("B"));
        // Peter - Pter == e(101) - t(116) = -15
        System.out.println("b.compareTo(\"Pter\"): " + b.compareTo("Pter"));
        System.out.println("------------------------------");

        // Devuelve el valor char en el índice especificado.
        System.out.println("b.charAt(0): " + b.charAt(0));
        System.out.println("b.charAt(1): " + b.charAt(1));
        System.out.println("b.charAt(b.length() - 1): " + b.charAt(b.length() - 1));
        System.out.println("------------------------------");

        // Devuelve una cadena que es una subcadena de esta cadena.
        // La subcadena comienza con el carácter en el índice especificado y se extiende hasta el final de esta cadena.
        System.out.println("b.substring(1): " + b.substring(1));
        System.out.println("b.substring(1, 4): " + b.substring(1, 4));
        System.out.println("b.substring(b.length() - 1): " + b.substring(b.length() - 1));
        System.out.println("------------------------------");


        String palabra = "palabra";
        // Reemplaza cada subcadena de esta cadena que coincida con la secuencia de destino literal con la secuencia de reemplazo literal especificada.
        // Genera una nueva salida, no reemplaza la variable.
        System.out.println("palabra:" + palabra.replace("a", "."));
        System.out.println("------------------------------");

        // Devuelve el índice dentro de esta cadena de la primera aparición de la subcadena especificada. >> indexOf("a")
        // Devuelve el índice dentro de esta cadena de la primera aparición del carácter especificado. >> indexOf(1)
        System.out.println("palabra.indexOf(\"a\"): " + palabra.indexOf("a"));
        System.out.println("palabra.indexOf(\"abra\"): " + palabra.indexOf("abra"));
        System.out.println("palabra.indexOf(1): " + palabra.indexOf(1));
        // Forma de detectar la extensión de un archivo.
        String extension = "file.pdf";
        System.out.println("Extensión del archivo: " + extension.substring(extension.indexOf(".") + 1));
        System.out.println("------------------------------");

        // Devuelve el índice dentro de esta cadena de la última aparición de la subcadena especificada.
        System.out.println("palabra.lastIndexOf(\"a\"): " + palabra.lastIndexOf("a"));
        System.out.println("------------------------------");

        // Devuelve verdadero si y solo si esta cadena contiene la secuencia especificada de valores de caracteres.
        System.out.println("palabra.contains(\"abra\"): " + palabra.contains("abra"));
        System.out.println("------------------------------");

        // Comprueba si esta cadena comienza con el prefijo especificado.
        System.out.println("palabra.startsWith(\"pa\"): " + palabra.startsWith("pa"));
        System.out.println("------------------------------");

        // Comprueba si esta cadena termina con el sufijo especificado.
        System.out.println("palabra.endsWith(\"ra\"): " + palabra.endsWith("ra"));
        System.out.println("------------------------------");

        // Devuelve una cadena cuyo valor es esta cadena, con todos los espacios iniciales y finales eliminados,
        // donde el espacio se define como cualquier carácter cuyo punto de código es menor o igual que 'U+0020' (el carácter de espacio).
        System.out.println("\" palabra \".trim(): " + " palabra ".trim());
        System.out.println("------------------------------");

        // Divide esta cadena en torno a las coincidencias de la expresión regular dada.
        String[] arrayCadena = "Texto con espacios.".split("\\s+");
        System.out.println("\"Texto con espacios.\".split(\" \"): " + Arrays.toString(arrayCadena));
        // Se puede conseguir el mismo resultado usando corchetes.
        String[] arrayCadena1 = "Texto con espacios.".split("[ ]");
        System.out.println("\"Texto con espacios.\".split(\" \"): " + Arrays.toString(arrayCadena1));
    }

}
