import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class DateCalendar {
    public static void main(String[] args) {

        // Creamos la instancia a la clase DATE.
        // Nos brinda la fecha actual.
        Date fecha1 = new Date();
        System.out.println("fecha1 = " + fecha1);

        // Llamamos a al método simpleDateFormatFunc.
        System.out.println("fechaFormato = " + simpleDateFormatFunc(fecha1, "yyyy.MM.dd G 'at' HH:mm:ss z"));
        System.out.println("=====================================");

        // Generamos un for con la idea de tener un tiempo de espera para poder armar una diferencia de tiempo.
        long aux = 0;
        for (int i = 0; i < 1000000000; i++) {
            aux += i;
        }

        Date fecha2 = new Date();
        //
        long diffFecha = fecha2.getTime() - fecha1.getTime();
        System.out.println("diffFecha = " + diffFecha);
        System.out.println("=====================================");

        //
        Calendar calendario = Calendar.getInstance();

        calendario.set(Calendar.YEAR, 2022);
        calendario.set(Calendar.MONTH, Calendar.JULY);
        calendario.set(Calendar.DAY_OF_MONTH, 23);
        calendario.set(Calendar.HOUR, 8);
        calendario.set(Calendar.AM_PM, Calendar.AM);
        calendario.set(Calendar.MINUTE, 20);
        calendario.set(Calendar.SECOND, 20);
        calendario.set(Calendar.MILLISECOND, 100);

        Date fechaCalendario = calendario.getTime();
        System.out.println("fechaCalendario = " + fechaCalendario);

        System.out.println("simpleDateFormatFunc(fechaCalendario, \"yyyy-MM-dd HH:mmss:SSS a\") = " + simpleDateFormatFunc(fechaCalendario, "yyyy-MM-dd HH:mmss:SSS a"));
        System.out.println("=====================================");

        //
        stringToDate();

    }

    // Generamos este método para que nos devuelva la fecha formateada.
    public static String simpleDateFormatFunc(Date fecha, String format) {
        // Utilizando la clase SimpleDateFormat podemos dar el formato que nosotros queremos a la fecha.
        // Podemos customizar el formato agregando texto simplemente con comillas simples >> 'at'
        // https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/text/SimpleDateFormat.html
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(fecha);
    }

    //
    public static void stringToDate() {
        Scanner s = new Scanner(System.in);
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

        System.out.println("Ingrese una fecha con formato 'yyyy-MM-dd'");
        try {
            Date fecha1 = formato.parse(s.next());
            System.out.println("fecha = " + fecha1);
            System.out.println("formato = " + formato.format(fecha1));

            Date fecha2 = new Date();
            System.out.println("fecha2 = " + fecha2);

            comparadorDeFechas(fecha1, fecha2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //
    private static void comparadorDeFechas(Date fecha1, Date fecha2) {
        //
        if (fecha1.after(fecha2)) {
            System.out.println("fecha1 es después de fecha2.");
        } else if (fecha1.before(fecha2)) {
            System.out.println("fecha1 es anterior a fecha2");
        } else if (fecha1.equals(fecha2)) {
            System.out.println("fecha1 es igual a fecha2");
        }

        //
        if (fecha1.compareTo(fecha2) > 0) {
            System.out.println("fecha1 es después de fecha2.");
        } else if (fecha1.compareTo(fecha2) < 0) {
            System.out.println("fecha1 es anterior a fecha2");
        } else if (fecha1.compareTo(fecha2) == 0) {
            System.out.println("fecha1 es igual a fecha2");
        }
    }
}
