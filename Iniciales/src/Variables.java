// Tipos de datos
public class Variables {

    // Los valores por defecto solo se aplican a atributos y variables de la clase.
    // No aplica a las variables del método por ejemplo variables locales del método main.
    // Para que el atributo pueda ser utilizado dentro de una clase o método estático el mismo también debe ser estático.
    static float varInicializada;


    // Tipear main + enter y se construye automáticamente la clase main
    public static void main(String[] args) {

        System.out.println("--------------- Primitivos ---------------");
        // Primitivos --------------------------
        // 1 bit
        System.out.println("--------------- boolean ---------------");
        boolean valor = true;
        System.out.println("valor = " + valor);
        System.out.println("------------------------------ \n");

        // Usa el código UNICODE y ocupa cada carácter de 16 bits
        // 16 bits
        // Siempre se define con comillas simples.
        System.out.println("--------------- char ---------------");
        char a = 'a';
        System.out.println("a = " + a);
        char b = 'b';
        System.out.println("b = " + b);
        char c = '\u0021';
        System.out.println("c = " + c);
        char dec = 64;
        System.out.println("dec = " + dec);
        // System.getProperty("line.separator") variable de ambiente de la maquina de java.
        char sim = '@';
        System.out.println("sim = " + System.getProperty("line.separator") + sim);
        System.out.println("sim = " + System.lineSeparator() + sim);
        System.out.println("------------------------------ \n");

        // Con 8 bits se pueden representar 256 números: {-128, ..... , 0 , ...... 127}
        System.out.println("--------------- byte ---------------");
        byte enteroByte = 127;
        System.out.println("enteroByte = " + enteroByte);
        System.out.println("Tipo byte corresponde en byte a = " + Byte.BYTES);
        System.out.println("Tipo byte corresponde en bytes a = " + Byte.SIZE);
        System.out.println("Valor máximo de un byte = " + Byte.MAX_VALUE);
        System.out.println("Valor mínimo de un byte = " + Byte.MIN_VALUE);
        System.out.println("------------------------------ \n");

        // Con 16 bits se pueden representar 65536 números: {-32768, ..... , 0 , ...... 32767}
        System.out.println("--------------- short ---------------");
        short enteroShort = 32767;
        System.out.println("enteroShort = " + enteroShort);
        System.out.println("Tipo short corresponde en byte a = " + Short.BYTES);
        System.out.println("Tipo short corresponde en bytes a = " + Short.SIZE);
        System.out.println("Valor máximo de un Short = " + Short.MAX_VALUE);
        System.out.println("Valor mínimo de un Short = " + Short.MIN_VALUE);
        System.out.println("------------------------------ \n");

        // Con 32 bits son 4.294.967.296 números: {-2.147.483.648,....,0,.....2.147.483.647}
        // La literal siempre es de tipo integer.
        System.out.println("--------------- int ---------------");
        int enteroInt = 2147483647;
        System.out.println("enteroInt = " + enteroInt);
        System.out.println("Tipo int corresponde en byte a = " + Integer.BYTES);
        System.out.println("Tipo int corresponde en bytes a = " + Integer.SIZE);
        System.out.println("Valor máximo de un Integer = " + Integer.MAX_VALUE);
        System.out.println("Valor mínimo de un Integer = " + Integer.MIN_VALUE);
        System.out.println("------------------------------ \n");

        // Con 64 bits se pueden representar estos: {-9.223.372.036.854.775.808,.,0,..+9.223.372.036.854.775.807}
        // Como la literal siempre es de tipo integer hay que asignarle una letra al final para indicar el nuevo tipo.
        System.out.println("--------------- long ---------------");
        long enteroLong = 9223372036854775807L;
        System.out.println("enteroLong = " + enteroLong);
        System.out.println("Tipo long corresponde en byte a = " + Long.BYTES);
        System.out.println("Tipo long corresponde en bytes a = " + Long.SIZE);
        System.out.println("Valor máximo de un Integer = " + Long.MAX_VALUE);
        System.out.println("Valor mínimo de un Integer = " + Long.MIN_VALUE);
        System.out.println("------------------------------ \n");

        // Entre 1.4E-45 a 3.4028235E38
        // 32 bits
        // La literal siempre es de tipo double para números con coma, por ende hay que indicarle el tipo.
        System.out.println("--------------- float ---------------");
        float realFloat = 3.1416f;
        System.out.println("realFloat = " + realFloat);
        System.out.println("Tipo float corresponde en byte a = " + Float.BYTES);
        System.out.println("Tipo float corresponde en bytes a = " + Float.SIZE);
        System.out.println("Valor máximo de un Float = " + Float.MAX_VALUE);
        System.out.println("Valor mínimo de un Float = " + Float.MIN_VALUE);
        System.out.println("------------------------------ \n");

        // Entre 4.9E-324 a 1.7976931348623157E308.
        // 64 bits
        System.out.println("--------------- double ---------------");
        double realDouble = 4.7029235E3;
        System.out.println("realDouble = " + realDouble);
        System.out.println("Tipo double corresponde en byte a = " + Double.BYTES);
        System.out.println("Tipo double corresponde en bytes a = " + Double.SIZE);
        System.out.println("Valor máximo de un Double = " + Double.MAX_VALUE);
        System.out.println("Valor mínimo de un Double = " + Double.MIN_VALUE);
        System.out.println("------------------------------ \n");

        // Sistemas numéricos
        System.out.println("--------------- Sistemas numéricos ---------------");
        int numeroDecimal = 888;
        System.out.println("numeroDecimal = " + numeroDecimal);
        // Conversíon a Binario
        System.out.println("numero binario de " + numeroDecimal + " = " + Integer.toBinaryString(numeroDecimal));
        int numeroBinario = 0b1101111000;
        System.out.println("numeroBinario = " + numeroBinario);

        // Conversíon a Octal
        System.out.println("numero Octal de " + numeroDecimal + " = " + Integer.toOctalString(numeroDecimal));
        int numeroOctal = 01570;
        System.out.println("numeroBinario = " + numeroOctal);

        // Conversíon a Hexa
        System.out.println("numero Haxadecimal de " + numeroDecimal + " = " + Integer.toHexString(numeroDecimal));
        int numeroHex = 0x378;
        System.out.println("numeroBinario = " + numeroHex);
        System.out.println("------------------------------ \n");


        // Caso de variables inicializadas
        System.out.println("varInicializada = " + varInicializada);

        // Objeto --------------------------
        // var es para una variable de tipo flexible.
        // Desde jdk 10 en adelante, no se soporta en Java 8
        var variable = "13";
        System.out.println("variable = " + variable);
        // l/L = long
        // f/F = float
        // d/D = double
        // La literal siempre es de tipo integer.
        var variableNumerica = 12345678901111111111123D;
        System.out.println("variableNumerica = " + variableNumerica);
        System.out.println("------------------------------ \n");

        System.out.println("--------------- string ---------------");
        String saludar = "Hola mundo";
        // Tipear sopv para que automáticamente agregue la impresión.
        System.out.println("saludar = " + saludar);
        System.out.println("------------------------------ \n");
    }
}