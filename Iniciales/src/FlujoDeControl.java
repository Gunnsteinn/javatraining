import java.util.Scanner;

public class FlujoDeControl {
    public static void main(String[] args) {

        // Ejercicio 1
        // Detección de palabras, con la finalidad de mostrar el funcionamiento de for anidados y etiquetas.
        String frase = "tres tristes tigres, tragaban trigo en un trigal, en tres tristes trastos, tragaban trigo tres tristes tigrestres";
        String palabra = "tres";

        int maxPalabra = palabra.length();
        int maxFrase = frase.length() - maxPalabra;
        int cantidad = 0;

        buscar:
        for (int i = 0; i <= maxFrase; ) {
            int k = i;
            for (int j = 0; j < maxPalabra; j++) {
                if (frase.charAt(k++) != palabra.charAt(j)) {
                    i++;
                    continue buscar;
                }
            }
            cantidad++;
            i += maxPalabra;
        }

        System.out.println("Encontrado = " + cantidad + " veces la palabra " + palabra + " en la frase\n\n");
        System.out.println("--------------------------------------------------------------------------\n\n");

        // Ejercicio 2
        // Mediante el teclado pedir dos números enteros positivos o negativos y multiplicarlos, pero sin usar el símbolo de multiplicación (*).
        Scanner numero1 = new Scanner(System.in);
        Scanner numero2 = new Scanner(System.in);

        System.out.println("Ingrese los números a multiplicar");
        System.out.println("Ingrese el numero a: ");
        double num1 = numero1.nextDouble();
        System.out.println("Ingrese el numero b: ");
        double num2 = numero2.nextDouble();
        double resultado = 0;

        boolean signoNum1 = num1 > -1;
        boolean signoNum2 = num2 > -1;

        for (int i = 0; i < Math.abs(num1); i++) {
            resultado += num2;
        }

        if (!(signoNum1 && signoNum2) || signoNum1) {
            resultado = -resultado;
        }

        System.out.println("resultado = " + resultado + "\n\n");
        System.out.println("--------------------------------------------------------------------------\n\n");

        // Ejercicio 3
        // Búsqueda de nombres en una lista preestablecida.
        String[] lista = {"Maria", "Veronica", "Pepe", "Juan", "Laura", "Guillermo", "Mariano"};
        Scanner palabra1 = new Scanner(System.in);

        System.out.println("Ingrese un nombre:");
        String pal1 = palabra1.nextLine();

        boolean encontrado = false;
        for (int i = 0; i < lista.length; i++) {
            if (lista[i].equalsIgnoreCase(pal1)) {
                encontrado = true;
                break;
            }
        }
        if (encontrado) {
            System.out.println("El nombre " + pal1 + " fue encontrado en la lista.");
        } else {
            System.out.println("El nombre " + pal1 + " no fue encontrado en la lista.");
        }

    }
}