public class Operadores {

    public static void main(String[] args) {

        int a = 20;
        int b = 10;
        int c = 0;
        int d = 20;
        int e = 40;
        int f = 30;
        int bitA = 0x0005;
        int bitB = 0x0007;
        int shiftA = 0x0005;
        int shiftB = -10;
        int result;

        Integer num = 50;
        String x = "Thank";
        String y = "Thank";

        int[] ar = {1, 2, 3};
        int[] br = {1, 2, 3};

        boolean condicion = true;

        System.out.println("a = 20, b = 10, c = 0, d = 20, e = 40, f = 30");
        System.out.println("x = \"Thank\", y = \"Thank\"");
        System.out.println("ar[] = {1, 2, 3} - br[] = {1, 2, 3}");

        // Operadores aritméticos
        System.out.println("--------------- Operadores aritméticos ---------------");

        // Operador + y -
        System.out.println("a + b = " + (a + b));
        System.out.println("a - b = " + (a - b));

        // El operador + si se usa con strings
        // concatena las cadenas dadas.
        System.out.println("x + y = " + x + y);

        // Operador * y /
        System.out.println("a * b = " + (a * b));
        System.out.println("a / b = " + (a / b));

        // operador de módulo da el resto
        // de dividir el primer operando con el segundo
        // si el denominador es 0 en la división
        // System.out.println(a/c);
        // lanzaría una java.lang.ArithmeticException
        System.out.println("a % b = " + (a % b));


        // Operador de asignación (=)
        System.out.println("--------------- Operador de asignación (=) ---------------");
        // operador de asignación simple
        c = b;
        System.out.println("Valor de c = " + c);

        // Esta siguiente declaración arrojaría una exception
        // porque el valor del operando derecho debe ser inicializado
        // antes de la asignación, entonces el programa no
        // compila.
        // c = d;
        // operadores de asignación simples
        a = a + 1;
        b = b - 1;
        e = e * 2;
        f = f / 2;
        System.out.println("a,b,e,f = " + a + "," + b + "," + e + "," + f);
        a = a - 1;
        b = b + 1;
        e = e / 2;
        f = f * 2;

        // operados de asignación compuestos/cortos
        a += 1;
        b -= 1;
        e *= 2;
        f /= 2;
        System.out.println("a,b,e,f (usando operadores cortos)= " + a + "," + b + "," + e + "," + f);


        // Operadores unarios
        System.out.println("--------------- Operadores unarios ---------------");
        // operador de pre-incremento
        // a = a+1 y entonces c = a;
        c = ++a;
        System.out.println("Valor de c (++a) = " + c);

        // operador de post-incremento
        // c=b entonces b=b+1 (b pasa a ser 11)
        c = b++;
        System.out.println("Valor de c (b++) = " + c);

        // operador de pre-decremento
        // d=d-1 entonces c=d
        c = --d;
        System.out.println("Valor de c (--d) = " + c);

        // operador de post-decremento
        // c=e entonces e=e-1 (e pasa a ser 39)
        c = e--;
        System.out.println("Valor de c (e--) = " + c);

        // Operador lógico not
        System.out.println("Valor de !condition = " + !condicion);


        System.out.println("--------------- Operadores relacionales ---------------");
        //varios operadores condicionales
        System.out.println("a == b :" + (a == b));
        System.out.println("a < b :" + (a < b));
        System.out.println("a <= b :" + (a <= b));
        System.out.println("a > b :" + (a > b));
        System.out.println("a >= b :" + (a >= b));
        System.out.println("a != b :" + (a != b));

        // Los Arrays no se pueden comparar con
        // operadores relacionales porque los objetos
        // almacenan referencias, mas no el valor
        System.out.println("x == y : " + (ar == br));
        System.out.println("condicion==true :" + (condicion == true));


        System.out.println("--------------- Operador ternario ---------------");
        // Operador ternario. (condición) ? valor1 : valor2
        System.out.println(2 < 3 ? 1 : 2);
        //el resultado obtiene el máximo de tres números.
        result = ((a > b) ? (a > c) ? a : c : (b > c) ? b : c);
        System.out.println("Máximo de tres números = " + result);


        System.out.println("--------------- Operadores lógicos ---------------");
        // NOT
        System.out.println("!(a > b): " + (!(a > b)));
        // AND
        System.out.println("(a > b) && (a > c): " + ((a > b) && (a > c)));
        // OR
        System.out.println("(a > b) || (a > c): " + ((a > b) || (a > c)));


        System.out.println("--------------- Operador de Instancia (instanceof) ---------------");
        // El operador de instancia se usa para verificar el tipo.
        // Se puede usar para probar si un objeto es una instancia de una clase, una subclase o una interfaz.
        System.out.println("x instanceof String: " + (x instanceof String));
        System.out.println("x instanceof Object: " + (x instanceof Object));
        System.out.println("num instanceof Object: " + (num instanceof Object));
        System.out.println("num instanceof Integer: " + (num instanceof Integer));
        System.out.println("num instanceof Number: " + (num instanceof Number));

        Object text = "Hola mundo";
        System.out.println("text instanceof String: " + (text instanceof String));
        System.out.println("text instanceof Object: " + (text instanceof Object));
        System.out.println("text instanceof Integer: " + (text instanceof Integer));
        System.out.println("text instanceof Number: " + (text instanceof Number));


        System.out.println("--------------- Operadores bit a bit ---------------");
        // bitwise AND
        // 0101 & 0111=0101
        System.out.println("a&b = " + (bitA & bitB));

        // bitwise or
        // 0101 | 0111=0111
        System.out.println("a|b = " + (bitA | bitB));

        // bitwise xor
        // 0101 ^ 0111=0010
        System.out.println("a^b = " + (bitA ^ bitB));

        // bitwise complemento
        // ~0101=1010
        System.out.println("~a = " + ~bitA);

        // también se puede combinar con el
        // operador de asignación
        // a=a&b
        bitA &= bitB;
        System.out.println("a= " + bitA);


        System.out.println("--------------- Operadores shift ---------------");
        // operador de desplazamiento a la izquierda
        // 0000 0101<<2 =0001 0100(20)
        // similar a 5*(2^2)
        System.out.println("a<<2 = " + (shiftA << 2));

        // operador de desplazamiento a la derecha
        // 0000 0101 >> 2 =0000 0001(1)
        // similar a 5/(2^2)
        System.out.println("a>>2 = " + (shiftA >> 2));

        // operador de cambio a la derecha sin firmar
        System.out.println("b>>>2 = " + (shiftB >>> 2));
    }
}






