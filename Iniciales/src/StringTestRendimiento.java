// Ejercicio para demostrar la performance de cada uno de los procesos de concatenación de cadenas de caracteres.

public class StringTestRendimiento {
    public static void main(String[] args) {

        String a = "a";
        String b = "b";
        String c = a;
        int contador = 50000;

        concatTest(a, b, c, contador);
        sumaTest(a, b, c, contador);
        stringBuilderTest(a, b, c, contador);

    }

    private static void concatTest(String a, String b, String c, int contador) {
        long inicio = System.currentTimeMillis();
        for (int i = 0; i < contador; i++) {
            c = c.concat(a).concat(b).concat("\n");
        }
        System.out.println(((double) (System.currentTimeMillis() - inicio) / 1000) % 60 + " seg");
    }

    private static void sumaTest(String a, String b, String c, int contador) {
        long inicio = System.currentTimeMillis();
        for (int i = 0; i < contador; i++) {
            c += a + b + "\n";
        }
        System.out.println(((double) (System.currentTimeMillis() - inicio) / 1000) % 60 + " seg");
    }

    private static void stringBuilderTest(String a, String b, String c, int contador) {
        long inicio = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder(a);
        for (int i = 0; i < contador; i++) {
            sb.append(a).append(b).append("\n");
        }
        System.out.println(((double) (System.currentTimeMillis() - inicio) / 1000) % 60 + " seg");
    }

}
