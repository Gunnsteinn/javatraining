// Obtener el nombre más largo de tres personas, según los siguientes requerimientos
// Mediante el teclado pedir el nombre completo (nombre + apellido) de tres miembros de la familia o amigos usando la clase JOptionPane y método showInputDialog().
// Calcular e Imprimir el nombre de la persona que tenga el nombre más largo (en cantidad de caracteres)
// (Imprimir solo uno de los tres, el de más caracteres en el nombre.)
// Podría usar .split(" "); del objeto String para separar nombre y apellido en un arreglo, y con el índice cero accedemos al nombre de la persona.
// Restricción no usar loops en el desarrollo de la tarea.
// Ejemplo del resultado: "Guillermo Doe tiene el nombre más largo."

import javax.swing.*;
import java.awt.*;

public class NombresEjercicio {
    public static void main(String[] args) {

        JTextField xField = new JTextField(5);
        JTextField yField = new JTextField(5);
        JTextField zField = new JTextField(5);

        JPanel myPanel = new JPanel(new GridLayout(0, 1, 2, 2));
        myPanel.add(new JLabel("Ingrese nombres y apellidos:"));
        myPanel.add(xField);
        myPanel.add(yField);
        myPanel.add(zField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                null, JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            String mayor = (xField.getText().split("[ ]")[0].length() > yField.getText().split("[ ]")[0].length()) ? xField.getText() : yField.getText();
            mayor = (mayor.length() > zField.getText().split("[ ]")[0].length()) ? mayor : zField.getText();
            JOptionPane.showMessageDialog(null, mayor + " tiene el nombre más largo.");
        }
    }
}
