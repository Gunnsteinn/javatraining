import java.util.InputMismatchException;
import java.util.Scanner;

// Ejercicio para demostrar el uso de la consola, tanto para ingresar valores como para mostrar mensajes.

public class SistemaDeEntradaPorConsola {

    public static void main(String[] args) {
        // Utilizo la clase Scanner para poder leer datos desde la consola.
        // Scanner espera recibir un argumento para el constructor que en este caso es el System.in = entrada de la consola
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un número: ");

        int numeroDecimalCon = 0;
        try {
            // Método para obtener la línea de la consola donde el usuario ingresa el valor.
            // Captura solo enteros, si no llega a ser este un int dispara una "InputMismatchException".
            numeroDecimalCon = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Error.\n" + e.getMessage());
            main(args);
            System.exit(0);
        }

        // Conversión a Binario
        String resultadoBin = "numero binario de " + numeroDecimalCon + " = " + Integer.toBinaryString(numeroDecimalCon) + "\n";

        // Conversión a Octal
        String resultadoOct = "numero Octal de " + numeroDecimalCon + " = " + Integer.toOctalString(numeroDecimalCon) + "\n";

        // Conversión a Hexa
        String resultadoHex = "numero Haxadecimal de " + numeroDecimalCon + " = " + Integer.toHexString(numeroDecimalCon) + "\n";

        // Vamos a mostrar los valores por consola.
        System.out.println(resultadoBin + resultadoOct + resultadoHex);
    }
}
