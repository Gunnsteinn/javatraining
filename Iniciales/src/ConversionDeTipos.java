public class ConversionDeTipos {
    public static void main(String[] args) {
        // String to int
        String numeroStr = "50";
        int numeroInt = Integer.parseInt(numeroStr);
        System.out.println("numeroStr = " + numeroInt);
        System.out.println(((Object) numeroInt).getClass().getSimpleName());

        // String to Double
        String realStr = "988834.4324e-4";
        double realDouble = Double.parseDouble(realStr);
        System.out.println("realStr = " + realDouble);
        System.out.println(((Object) realDouble).getClass().getSimpleName());

        // String to boolean
        String logicoStr = "TruE";
        boolean logicoBoolean = Boolean.parseBoolean(logicoStr);
        System.out.println("logicoStr = " + logicoBoolean);
        System.out.println(((Object) logicoBoolean).getClass().getSimpleName());

        // int to String 1er caso
        int numberInt = 100;
        String intToString1 = Integer.toString(numberInt);
        System.out.println("intToString1 = " + intToString1);

        // int to String 2do caso
        String intToString2 = String.valueOf(numberInt);
        System.out.println("intToString2 = " + intToString2);

        // double to String 1er caso
        double numberDouble1 = 10.1234e4;
        String doubleToString1 = Double.toString(numberDouble1);
        System.out.println("doubleToString1 = " + doubleToString1);

        // double to String 2do caso
        double numberDouble2 = 10.1234e4;
        String doubleToString2 = String.valueOf(numberDouble2);
        System.out.println("doubleToString2 = " + doubleToString2);

        // Se puede realizar un cast para convertir valores
        int i = 1000;
        short s = (short) i;
        System.out.println("s = " + s);
        
    }
}