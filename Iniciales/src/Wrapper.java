import java.lang.reflect.Method;

public class Wrapper {
    public static void main(String[] args) {
        // Las clases Wrapper representa a un primitivo en forma de objeto,
        // es decir, envuelven, anidan este valor primitivo para dar mayor funcionalidad, método de comprobación,
        // método para validar y conversiones de datos a tipos compatibles.
        int intPrimitivo = 15;
        // Boxing >> Integer.valueOf(intPrimitivo)
        // Estamos convirtiendo un primitivo a un objeto Integer
        Integer intObjeto1 = Integer.valueOf(intPrimitivo);
        // Sin Boxing.
        Integer intObjeto2 = intPrimitivo;
        System.out.println("intObjeto1 es de tipo = " + intObjeto1.getClass().getSimpleName());
        System.out.println("intObjeto2 es de tipo = " + intObjeto2.getClass().getSimpleName());

        Integer[] arrayEnteros = {Integer.valueOf(1), 2, 3, 4, 5, 6, 7, 8, 9};

        // Detección de pares.
        // La idea es demostrar como se puede generar el unBoxing de un Objeto para que se convierta en un primitivo
        for (Integer arrayEntero : arrayEnteros) {
            if ((arrayEntero.intValue() % 2) == 0) {
                System.out.println("arrayEntero = " + arrayEntero);
            }
        }

        // Método getClass
        String texto = "Hola mundo";
        Class strClass = texto.getClass();

        System.out.println("strClass.getName() = " + strClass.getName());
        System.out.println("strClass.getSimpleName() = " + strClass.getSimpleName());
        System.out.println("strClass.getPackageName() = " + strClass.getPackageName());
        System.out.println("strClass.getName() = " + strClass);

        for (Method metodo : strClass.getMethods()) {
            System.out.println("metodo.getName() = " + metodo.getName());
        }


        Integer num = 20;
        Class intClass = num.getClass();
        // Integer es el objeto, pero extiende de Number y number extiende de Object.
        // Con el método Superclass podemos ver de donde extienden.
        Class objClass = intClass.getSuperclass().getSuperclass();
        System.out.println("intClass.getSuperclass().getSuperclass() = " + intClass.getSuperclass());
        System.out.println("objClass.getSuperclass().getSuperclass() = " + objClass);

        for (Method metodo: objClass.getDeclaredMethods()) {
            System.out.println("metodo.getName() = " + metodo.getName());
        }
    }
}
