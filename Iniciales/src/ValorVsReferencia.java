public class ValorVsReferencia {
    public static void main(String[] args) {
        // Pasar por valor
        // Toda variable primitiva siempre se pasa por valor, ya que no es un objeto al cual se puede referenciar.
        int a = 10;
        Integer b = 15;
        int[] edad = {12, 13, 14};

        printPrimitivo(a);
        System.out.println("Valor inicial de a = " + a);
        System.out.println("==========================");
        printClase(b);
        System.out.println("Valor inicial de b = " + b);
        System.out.println("==========================");
        System.out.println("{12, 13, 14}");
        printArreglo(edad);
        for (int j : edad) {
            System.out.println("Valor inicial de b = " + j);
        }
    }

    // Método al cual le pasamos un parámetro por valor
    // Cuando se pasa por valor, solamente estamos pasando por valor y no se modifica el primitivo creado en la clase main.
    public static void printPrimitivo(int a) {
        System.out.println("Parámetro por valor de a = " + a);
        a = 12;
        System.out.println("Nuevo valor de a = " + a);
    }

    // Método al cual le pasamos un parámetro por referencia pero es inmutable.
    // Si asignamos un valor a "b" se crea una nueva instancia por ende no se modifica la instancia de la clase main.
    public static void printClase(Integer b) {
        System.out.println("Parámetro por referencia de b = " + b);
        b = 12;
        System.out.println("Nuevo valor de b = " + b);
    }

    // Método al cual le pasamos un parámetro por referencia mutable.
    //
    public static void printArreglo(int[] a) {
        for (int i = 0; i < a.length; i++) {
            a[i] += 10;
        }
    }
}