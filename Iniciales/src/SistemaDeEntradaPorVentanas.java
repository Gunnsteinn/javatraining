import javax.swing.*;

// Ejercicio para demostrar el uso de las ventanas de diálogo, tanto para ingresar valores como para mostrar mensajes.

public class SistemaDeEntradaPorVentanas {

    public static void main(String[] args) {

        int numeroDecimalDia = 0;
        try {
            // JOptionPane.showInputDialog >> se abre una ventana que permite ingresar valores y capturarlos.
            numeroDecimalDia = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese un número entero."));
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Error.\n" + e.getMessage());
            main(args);
            System.exit(0);
        }

        // Conversíon a Binario
        String resultadoBin = "numero binario de " + numeroDecimalDia + " = " + Integer.toBinaryString(numeroDecimalDia) + "\n";

        // Conversíon a Octal
        String resultadoOct = "numero Octal de " + numeroDecimalDia + " = " + Integer.toOctalString(numeroDecimalDia) + "\n";

        // Conversíon a Hexa
        String resultadoHex = "numero Haxadecimal de " + numeroDecimalDia + " = " + Integer.toHexString(numeroDecimalDia) + "\n";

        // JOptionPane.showMessageDialog >> se abre una ventana que permite mostrar valores.
        JOptionPane.showMessageDialog(null, resultadoBin + resultadoOct + resultadoHex);
    }
}
