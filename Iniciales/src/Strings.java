public class Strings {
    public static void main(String[] args) {
        System.out.println("--------------- Análisis de Objetos ---------------");
        // Se genera la variable a la que se le asigna una cadena de caracteres.
        // Creación del objeto en forma literal
        String objeto1 = "Hola mundo";
        // Se utiliza la clase String a la cual por constructor se le pasa valores para inicializarla.
        String objeto2 = new String("Hola mundo");

        // Comparación de instancias de objetos
        System.out.println("Son objetos iguales = " + (objeto1 == objeto2));

        // Comparación por valor.
        System.out.println("Son valores iguales = " + objeto1.equals(objeto2));

        // Comparación por valor sin importar las mayúsculas.
        System.out.println("Son valores iguales = " + objeto1.equalsIgnoreCase("HoLa MuNdO"));

        // Si se crea de forma literal otro objeto con el mismo valor que ya se ha creado anteriormente(caso1), java asigna la referencia del primer objeto al segundo.
        String objeto3 = "Hola mundo";
        System.out.println("Son objetos iguales = " + (objeto3 == objeto1));

        // Concatenación y precedencia.
        System.out.println("--------------- Concatenación y precedencia ---------------");
        int a = 5;
        int b = 12;
        String c = "Java";
        String cadena = " Tutorial " + c + " con Willy ";
        System.out.println(cadena + a + b);
        System.out.println(a + b + cadena);
        System.out.println("Tutorial de ".concat(c));


        // Inmutabilidad.
        System.out.println("--------------- Inmutabilidad ---------------");
        String cadena1 = "Hola mundo";
        String cadena2 = " de Java";

        // Se crea dos objetos para demostrar que no puede mutar el primero usando clases como concat.
        String resultado = cadena1.concat(cadena2);
        System.out.println("cadena1 = " + cadena1);
        System.out.println("resultado = " + resultado);
        System.out.println(cadena1 == resultado);

        // Otra forma de concatenar un string de forma más funcional es utilizando el método transform que usa funciones lambdas(función de flecha).
        // transform es aplicable para versiones superiores a Java 11.
        // transform recibe como argumento funciones, en este caso se le pasa una función lambda.
        String resultado2 = cadena1.transform(p -> {
            return p + cadena2 + " con willy.";
        });

        // cadena1 siempre se mantiene inmutable.
        System.out.println("cadena1 = " + cadena1);
        System.out.println("resultado2 = " + resultado2);

        String resultado3 = resultado.replace("a", "A");
        // resultado siempre se mantiene inmutable.
        System.out.println("resultado = " + resultado);
        System.out.println("resultado3 = " + resultado3);
    }
}
