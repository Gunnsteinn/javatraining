import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ClaseSystem {
    public static void main(String[] args) throws IOException {

        // https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html
        String username = System.getProperty("user.name");
        System.out.println("username = " + username);

        String home = System.getProperty("user.home");
        System.out.println("home = " + home);

        String workspace = System.getProperty("user.dir");
        System.out.println("workspace = " + workspace);

        String java = System.getProperty("java.version");
        System.out.println("java = " + java);

        String lineSeparator1 = System.getProperty("line.separator");
        System.out.println("lineSeparator1:" + lineSeparator1 + "Una linea nueva.");
        String lineSeparator2 = System.lineSeparator();
        System.out.println("lineSeparator2:" + lineSeparator2 + "Una linea nueva.");

        Properties p = System.getProperties();
        p.list(System.out);

        try {
            FileInputStream archivo = new FileInputStream("src/config.properties");

            Properties pf = new Properties(System.getProperties());
            pf.load(archivo);
            pf.setProperty("mi.propiedad.personalizada", "Mi valor que voy a guardar en el objeto properties");
            System.setProperties(pf);

            Properties pss = System.getProperties();
            System.out.println("pss = " + pss.getProperty("mi.propiedad.personalizada"));
            System.out.println(System.getProperty("config.puerto.servidor"));
            System.out.println(System.getProperty("config.user"));
            System.out.println(System.getProperty("config.lastname"));

            pss.list(System.out);
        } catch (Exception e) {
            System.out.println("e = " + e);
        }

    }
}
